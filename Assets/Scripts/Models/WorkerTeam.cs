﻿using Assets.Scripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class WorkerTeam
    {
        public Worker Worker {get;set;}
        public Team TeamOfTheWorker { get; set; }  
    }
}
