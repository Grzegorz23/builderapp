﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class ServiceCalculation
    {
        public Service Service { get; set; }
        public Calculation Calculation { get; set; }
    }
}
