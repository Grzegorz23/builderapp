﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class Job
    {
        public ServiceType JobServiceType { get; set; }
        public Product ProductUsedInJob { get; set; }
        public double SurfaceOfJob { get; set; }
        public double AmountOfProductInUnit { get; set; }

    }
}
