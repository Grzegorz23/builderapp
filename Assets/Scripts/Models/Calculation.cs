﻿using Assets.Scripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class Calculation
    {

        public List<ServiceCalculation> ListOfServices { get; set; }
        public string CalculationIdentifier { get; set; }

    }
}
