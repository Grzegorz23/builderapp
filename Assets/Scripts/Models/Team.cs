﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class Team
    {
        public long TeamIdentifier { get; set; }
        public List<WorkerTeam> WorkersList { get; set; }
        public double PriceForHour { get; set; }
    }
}
