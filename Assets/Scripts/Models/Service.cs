﻿using Assets.Scripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class Service
    {
        public string ServiceIdentifier { get; set; }
        public List<ServiceCalculation> CalculationsRelatedWithService { get; set; }
        public Team TeamOfWorkers { get; set; }
        public List<Job>   ListOfJobsToDoInService { get; set; }
        public double TotalPriceOfService { get; set; }
    }
}
