﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Product
{
    public string ProductName { get; set; }
    public Color ProductColor { get; set; }
    public double PriceForSquareMeter { get; set; }
    public double PriceForKg { get; set; }
}
