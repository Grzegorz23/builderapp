﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worker 
{
    public string WorkerIdentifier { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
    public double PriceForHour { get; set; }
    public List<WorkerTeam> WorkerTeams { get; set; }


}
